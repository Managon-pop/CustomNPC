# CustomNPC
##Custom NPC in your Server!!
NPC sends you Item and does command.<br />
You can set Item and Command.
###Usage
###How to set up
###Todo
###Entity ID List

##Usage
If you want to add Player's NPC, enter command in game <br />
<strong>/cn add player &lt;ItemId&gt; &lt;command(If you don't want, enter cn)&gt; &lt;Name&gt;</strong>
then tap the ground.<br /><br />
Or If you want to add Entity NPC, enter command<br />
<strong>/cc add &lt;ENTITY LIST&gt; &lt;ItemID&gt;</strong>
<br />If you delete NPC, enter
<br /><strong>/cn del</strong>&nbsp;and Tap.
##How to set up Item and command?
Open NPC.json and follow.<br />
<img src="https://github.com/Managon-pop/CustomNPC/blob/master/img/picc.png"></img>
<br />If you want to change command, change at "command". Like this,
<img src="https://github.com/Managon-pop/CustomNPC/blob/master/img/co.png"></img>
<br />And then go to your server and tap NPC.<br />
<img src="https://github.com/Managon-pop/CustomNPC/blob/master/img/nana.jpg"></img><br />
And If you want to change its name, change at "name".
##Todo
<ul>
<li>Change options of npc in game.</li>
<li>Does some commands(now only 1)</li>
<li>NPC always look player</li>
<li>Message</li>
</ul>
##Entity ID LIST
Arrow : 80<br />Bat : 19<br />Blaze : 43<br />Boat : 90<br />CaveSpider : 40<br />Chicken : 10<br />Cow : 11<br />Creeper : 33<br />Enderman : 38<br />Ghast : 41<br />IronGolem : 20<br />Mooshroom : 16<br />Ocelot : 22<br />Pig : 12<br />PigZombie : 36<br />Rabbit : 18<br />Sheep : 13<br />SilverFish : 39<br />Skelton : 34<br />SnowBall : 81<br />SnowGolem : 21<br />Spider : 35<br />Squid : 17<br />Villager : 16<br />Wolf : 14<br />Zombie : 32<br />ZombieVillager : 44
